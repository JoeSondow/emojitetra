package sondow.tetra.io;

import com.sybit.airtable.Airtable;
import com.sybit.airtable.Base;
import com.sybit.airtable.Query;
import com.sybit.airtable.Table;
import com.sybit.airtable.exception.AirtableException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import sondow.tetra.conf.BotConfig;
import sondow.tetra.conf.Logger;
import sondow.tetra.conf.StopWatch;

public class AirtableDatabase implements Database {

    private final static Logger log = Logger.getLogger(AirtableDatabase.class);
    private final BotConfig config;
    private Airtable airtable;
    private Table<AirtableRecord> table;
    private AirtableRecord record;

    AirtableDatabase(Airtable airtable, BotConfig config) {
        this.config = config;
        this.airtable = airtable;
    }

    public AirtableDatabase(BotConfig config) {
        this.config = config;
    }

    public static void main(String[] args) {
        // AirtableDatabase database = new AirtableDatabase(new BotConfigFactory().configure());
        // String state = database.readGameState();
        // log.info(state);
        // database.writeGameState("boom");
    }

    @Override public String readGameState() {
        String state;
        AirtableRecord record = getAirtableRecord();
        state = record.getState();
        if (state != null && state.length() <= 0) {
            state = null;
        }
        return state;
    }

    private AirtableRecord getAirtableRecord() {
        if (record == null) {
            String screenName = config.getWritingTwitterConfig().getUser();
            Query query = new AirtableBotStateQuery(screenName);
            List<AirtableRecord> results;
            try {
                results = selectResults(query);
                if (results.size() <= 0) {
                    createRecord(screenName);
                    results = selectResults(query);
                }
            } catch (AirtableException e) {
                throw new RuntimeException(e);
            }

            assert results.size() == 1;
            record = results.get(0);
            log.info("Reading game state from database: " + record.getState());
        }
        return record;
    }

    private List<AirtableRecord> selectResults(Query query) throws AirtableException {
        Table<AirtableRecord> table = getTable();
        StopWatch stopWatch = StopWatch.start("Airtable", "select");
        List<AirtableRecord> results = table.select(query);
        stopWatch.stopAndReport();
        return results;
    }

    private void createRecord(String screenName) {
        Table<AirtableRecord> table = getTable();
        AirtableRecord record = new AirtableRecord();
        record.setName(screenName);
        //noinspection TryWithIdenticalCatches because multi-catch Java code fails on AWS Lambda
        try {
            StopWatch stopWatch = StopWatch.start("Airtable", "create record");
            table.create(record);
            stopWatch.stopAndReport();
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private Table<AirtableRecord> getTable() {
        if (table == null) {
            Base base;
            try {
                if (airtable == null) {
                    log.info("Configuring Airtable");
                    airtable = new Airtable().configure(config.getAirtableAccessToken());
                }
                log.info("Getting Airtable Base");
                base = airtable.base(config.getAirtableBaseId());
            } catch (AirtableException e) {
                throw new RuntimeException(e);
            }
            StopWatch stopWatch = StopWatch.start("Airtable", "get table from base");
            table = base.table("TETRA", AirtableRecord.class);
            stopWatch.stopAndReport();
        }
        return table;
    }

    @Override public void writeGameState(String state) {
        Table<AirtableRecord> table = getTable();
        AirtableRecord record = getAirtableRecord();
        record.setState(state);
        //noinspection TryWithIdenticalCatches because multi-catch Java code fails on AWS Lambda
        try {
            StopWatch stopWatch = StopWatch.start("Airtable", "update record");
            log.info("Writing state to database: " + state);
            table.update(record);
            stopWatch.stopAndReport();
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void deleteGameState() {
        writeGameState("");
    }
}
