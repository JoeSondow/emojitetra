package sondow.tetra.conf

import java.time.ZonedDateTime
import spock.lang.Specification
import spock.lang.Unroll

class TipsSpec extends Specification {

    static File rota = new FileClerk().getFile("emojitetrarotations.gif")

    def 'no tips should be longer than 280 characters'() {
        setup:
        Tips tips = new Tips()

        when:
        List<String> allMessages = tips.getAllMessages()

        then:
        allMessages.size() >= 24
        for (String msg : allMessages) {
            verifyAll {
                msg.length() < 280
            }
        }
    }

    def 'should indicate if it is time to tweet a tip message'() {
        setup:
        Tips tips = new Tips()

        expect:
        tips.isTimeForTipMessage(ZonedDateTime.parse(time), interval) == result

        where:
        time                   | interval | result
        '2018-05-06T00:02:00Z' | 20       | false
        '2018-05-06T01:02:00Z' | 20       | false
        '2018-05-06T02:02:00Z' | 20       | false
        '2018-05-06T03:02:00Z' | 20       | false
        '2018-05-06T04:02:00Z' | 20       | false
        '2018-05-06T05:02:00Z' | 20       | false
        '2018-05-06T06:02:00Z' | 20       | false
        '2018-05-06T07:02:00Z' | 20       | false
        '2018-05-06T08:02:00Z' | 20       | false
        '2018-05-06T09:02:00Z' | 20       | false
        '2018-05-06T10:02:00Z' | 20       | false
        '2018-05-06T11:02:00Z' | 20       | false
        '2018-05-06T12:02:00Z' | 20       | false
        '2018-05-06T13:02:00Z' | 20       | false
        '2018-05-06T14:02:00Z' | 20       | false
        '2018-05-06T15:02:00Z' | 20       | false
        '2018-05-06T16:02:00Z' | 20       | false
        '2018-05-06T17:02:00Z' | 20       | false
        '2018-05-06T18:02:00Z' | 20       | false
        '2018-05-06T19:02:00Z' | 20       | false
        '2018-05-06T20:02:00Z' | 20       | true
        '2018-05-06T20:22:00Z' | 20       | false
        '2018-05-06T20:42:00Z' | 20       | false
        '2018-05-06T21:02:00Z' | 20       | false
        '2018-05-06T22:02:00Z' | 20       | false
        '2018-05-06T23:02:00Z' | 20       | false
        '2018-05-01T15:02:00Z' | 20       | true
        '2018-05-02T16:02:00Z' | 20       | true
        '2018-05-03T17:02:00Z' | 20       | true
        '2018-05-04T18:02:00Z' | 20       | true
        '2018-06-05T19:02:00Z' | 20       | true
        '2018-06-06T20:02:00Z' | 20       | true
        '2018-06-07T21:02:00Z' | 20       | true
    }

    def 'hour is variable but deterministic and between 14 and 22'() {
        setup:
        Tips tips = new Tips()

        expect:
        tips.getHourFor(date) == hour

        where:
        date | hour
        1    | 15
        2    | 16
        3    | 17
        4    | 18
        5    | 19
        6    | 20
        7    | 21
        8    | 22
        9    | 14
        10   | 15
        11   | 16
        12   | 17
        13   | 18
        14   | 19
        15   | 20
        16   | 21
        17   | 22
        18   | 14
        19   | 15
        20   | 16
        21   | 17
        22   | 18
        23   | 19
        24   | 20
        25   | 21
        26   | 22
        27   | 14
        28   | 15
        29   | 16
        30   | 17
        31   | 18
        32   | 19
        33   | 20
    }

    @Unroll
    def 'returns messages based on number'() {
        setup:
        Tips tips = new Tips()

        expect:
        TweetContent tweetContent = tips.getMessageForDayOfMonth(num)
        tweetContent.message == message
        tweetContent.file == file

        where:
        num | file | message
        1   | null |
                "This game is maintained by @JoeSondow who also tries to make Twitter less of a " +
                "nightmare with accounts like @EmojiSnakeGame @EmojiAquarium @EmojiMeadow " +
                "@PicardTips @RikerGoogling @WorfEmail. You can support these projects by " +
                "becoming a patron at patreon.com/JoeSondow"
        2   | null |
                "Game tip\n\nBefore you vote, check the side of the tweet to see what shape the " +
                "Next piece is. Think about how you want to use both the current and the next " +
                "piece together."
        3   | null |
                "Game tip\n\nYou can see all comments to all recent @EmojiTetra polls by " +
                "searching" +
                " twitter with this link: https://twitter" +
                ".com/search?q=emojitetra%20-from%3Aemojitetra&f=tweets&vertical=default&src=typd"
        4   | null |
                "Game tip\n\nClearing multiple lines in one move earns more points than clearing " +
                "the same number of lines one at a time."
        5   | null |
                "Game tip\n\nIf there are comments below one of my tweets, read the comments (yes" +
                " " +
                "really) for strategy ideas before you vote."
        6  | null | "When addressing players of this game collectively, make an effort to use " +
                "inclusive terms like “fam”, “folks”, “team”, “gang”, “crew”, “players”, " +
                "“people”, “y’all”, “buddies”, “pals”, “hivemind”, “peeps”, or “friends” rather " +
                "than “boys”, “guys”, “dudes”, “lads”, or “fellas”."
        7   | null |
                "If you want to drop some money in the tip jar for the implementer of this game " +
                "but you don't want recurring patreon payments, you can use paypal.me/JoeSondow " +
                "or ko-fi.com/JoeSondow\n\nTips are never required but much appreciated. Thank " +
                "you!"
        8   | null |
                "Game tip\n\nThe most common order of moves for a given piece is rotate, sideways" +
                " " +
                "movement, then down and plummet. If you're in agreement with others about the " +
                "piece's destination, this order is your best bet to avoid vote splitting."
        9   | null |
                "Game tip\n\nThis game is one of those rare places on the internet where it’s " +
                "helpful to read the comments. Some players draw diagrams showing clever " +
                "potential uses of the current and next pieces."
        10  | null |
                "Game tip\n\nThis game grid is smaller than what you’re probably used to. Try to " +
                "keep the top four rows clear enough to be able to rotate a 1x4 piece."
        11  | null |
                "Game tip\n\nWhen choosing where to drop a piece, consider what \"silhouette\" or" +
                " \"skyline\" will result at the bottom. The safest situation is one where there " +
                "is a good place to put any of the possible shapes."
        12 | null |
                "Player Code of Conduct v2:\n\nDon’t threaten or harass other players, even in " +
                "jest, even with images or emojis, on Twitter or elsewhere.\n\nBe respectful" +
                ".\n\nMake the comments more fun for other people, not more unpleasant.\n\nThis " +
                "is a non-exhaustive list. I block whomever I choose."
        13 | null |
                "This game is and always will be free, but if you want to tip the implementer, " +
                "you can do so at patreon.com/JoeSondow\n\nTips are never required and always " +
                "appreciated. Thank you very much!"
        14 | null |
                "Game tip\n\nThis game is all text. You can copy, paste, and edit the game board " +
                "in a reply to illustrate to other players what strategy you recommend."
        15 | null |
                "Game tip\n\nIt's safe to cover a hole if the next piece will uncover that hole " +
                "in an obvious way."
        16 | null |
                "When drawing your recommendation with emojis, avoid 1️⃣ and 2️⃣ because they " +
                "don’t render properly on some devices. Try one of these emoji pairs " +
                "instead:\n\n🥇🥈\n🔴🔵"
        17 | null | "FAQ: https://gitlab.com/JoeSondow/emojitetra/blob/master/README.md"
        18 | null |
                "We have a high energy group of players, which is great, but I want to avoid " +
                "toxic behavior in this community. If you see anything in the comments that makes" +
                " you uncomfortable, DM me about it."
        19 | null |
                "This game is the product of hundreds of hours of work, with plenty more still to" +
                " do. If you'd like to support development of new features, fixes, as" +
                " well as more Twitter games in the future, please consider becoming a patron: " +
                "patreon.com/JoeSondow"
        20 | null |
                "Twitter's poll system glitches occasionally and doesn't show the polls. If that " +
                "happens, try a different browser or Twitter client, or wait a few minutes."
        21 | null |
                "How Scoring Works\n\nSingle line cleared +100 points\n2 lines in one move +250 " +
                "points\n3 lines in one move +525 points\n4 lines in one move +1000 " +
                "points\n\nFull board clear +1000 points\n\nhttps://gitlab" +
                ".com/JoeSondow/emojitetra/blob/master/README.md#how-does-the-score-board-work"
        22 | rota | "Here's a gif showing the rotation rules for the shapes that rotate."
        23 | null | "Game Tip\n\nThe current shape choosing algorithm is completely random. The " +
                "shapes that came recently have no bearing on which shapes to expect soon."
        24 | null |
                "Game Tip\n\nThe game usually ends as a result of a combination of greed for " +
                "points, hope for a 1x4 piece, and bad luck. Go for risky fancy 4-line clears " +
                "near the bottom, not the top."
        25 | null |
                "🎵 If you’re angry and you show it, you get blocked 👏👏\n🎵 If you’re angry and" +
                " you show it, you get blocked 👏👏\n🎵 If you’re angry and you show it, and you " +
                "really want to blow it\n🎵 If you’re angry and you show it, you get blocked " +
                "👏👏"
        26  | null |
                "You can help support this game by retweeting its game polls to help grow the " +
                "player base. Retweets are much appreciated."
        27  | null |
                "Game tip\n\nIf you need to build towers to avoid burying holes, try to build " +
                "them against the far left or right side, not in the middle."
        28  | null |
                "Game tip\n\nKeep your eye on the \"Next\" indicator on the side of each game " +
                "tweet."
        29  | null |
                "Game tip\n\nSometimes it's possible to put a piece where it looks as if it will " +
                "cover a gap but instead it will instantly vanish" +
                ".\n\n◽🍕🍕🍕🍕◽◽\n◽◽◽◽◽◽🍕\n🍔◽◽◽◽🌮🍕\n🍔🍔◽◽◽🌮🍕\n🍔🍟🍟◽◽🌮🍕"
        30  | null |
                "Game tip\n\nLonger games score higher. If you're patient and your goal is a high" +
                " score, play it safe. If your goal is more frequent bursts of dopamine and the " +
                "high score is less important to you, go for the high risk, high reward tactics."
        31  | null |
                "Game tip\n\nThere is currently no gravity in this game. Pieces descend only when" +
                " Down or Plummet wins a poll. Rotations and lateral movements are unlimited."
        32  | null |
                "This game is maintained by @JoeSondow who also tries to make Twitter less of a " +
                "nightmare with accounts like @EmojiSnakeGame @EmojiAquarium @EmojiMeadow " +
                "@PicardTips @RikerGoogling @WorfEmail. You can support these projects by " +
                "becoming a patron at patreon.com/JoeSondow"
        33  | null |
                "Game tip\n\nBefore you vote, check the side of the tweet to see what shape the " +
                "Next piece is. Think about how you want to use both the current and the next " +
                "piece together."
        34  | null |
                "Game tip\n\nYou can see all comments to all recent @EmojiTetra polls by searching" +
                " twitter with this link: https://twitter" +
                ".com/search?q=emojitetra%20-from%3Aemojitetra&f=tweets&vertical=default&src=typd"
    }
}
