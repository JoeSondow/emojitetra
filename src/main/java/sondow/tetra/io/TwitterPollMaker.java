package sondow.tetra.io;

import java.io.File;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sondow.tetra.conf.BotConfig;
import sondow.tetra.conf.BotConfigFactory;
import sondow.tetra.conf.Logger;
import sondow.tetra.conf.StopWatch;
import sondow.tetra.conf.Time;
import sondow.tetra.retry.Retriable;
import twitter4j.Card;
import twitter4j.CardLight;
import twitter4j.CardsTwitterImpl;
import twitter4j.Choice;
import twitter4j.HttpResponse;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.StatusUpdateWithCard;
import twitter4j.StatusWithCard;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.auth.Authorization;
import twitter4j.auth.AuthorizationFactory;
import twitter4j.conf.Configuration;

/**
 * See JavaScript example at https://gist.github.com/fourtonfish/816c5272c3480c7d0e102b393f60bd49.
 * <p>
 * See Python example at https://gist.github.com/fourtonfish/5ac885e5e13e6ca33dca9f8c2ef1c46e
 * <p>
 * More reference to understand the examples above.
 * <p>
 * Endpoints: https://github.com/ttezel/twit/blob/master/lib/endpoints.js
 * <p>
 * Endpoints are also available in twitter4j at twitter4j.conf.ConfigurationBase but
 * caps.twitter.com is missing because it isn't a publicly documented part of Twitter's API.
 * <p>
 * JavaScript promise library Q: https://github.com/kriskowal/q
 *
 * @author @JoeSondow
 */
public class TwitterPollMaker {

    private final static Logger log = Logger.getLogger(TwitterPollMaker.class);

    private Time time;

    private Configuration pollReadingConfig;
    private CardsTwitterImpl pollReadingTwitter;

    private Configuration pollWritingConfig;
    private CardsTwitterImpl pollWritingTwitter;

    /**
     * For unit testing.
     */
    TwitterPollMaker(Configuration pollReadingConfig, CardsTwitterImpl pollReadingTwitter,
            Configuration pollWritingConfig, CardsTwitterImpl pollWritingTwitter, Time time) {
        this.pollReadingConfig = pollReadingConfig;
        this.pollReadingTwitter = pollReadingTwitter;
        this.pollWritingConfig = pollWritingConfig;
        this.pollWritingTwitter = pollWritingTwitter;
        this.time = time;
    }

    /**
     * For production.
     */
    public TwitterPollMaker(Configuration pollReadingConfig, Configuration pollWritingConfig,
            Time time) {

        this.pollReadingConfig = pollReadingConfig;
        Authorization pollReadingAuth = AuthorizationFactory.getInstance(pollReadingConfig);
        this.pollReadingTwitter = new CardsTwitterImpl(pollReadingConfig, pollReadingAuth);

        this.pollWritingConfig = pollWritingConfig;
        Authorization pollWritingAuth = AuthorizationFactory.getInstance(pollWritingConfig);
        this.pollWritingTwitter = new CardsTwitterImpl(pollWritingConfig, pollWritingAuth);

        this.time = time;
    }

    /**
     * If tweet attempt fails due to duplicate text, then delete existing dupes and try again.
     *
     * @param text              the text to tweet
     * @param inReplyToStatusId the id of the tweet to reply to, or null or -1 if not replying
     * @param file              optional image to add to tweet
     * @return the tweet that has been created
     */
    public Status tweet(String text, Long inReplyToStatusId, File file) {
        StatusUpdate update = new StatusUpdate(text);
        update.setMedia(file);
        if (inReplyToStatusId != null && inReplyToStatusId > 0) {
            update.setInReplyToStatusId(inReplyToStatusId);
        }
        return new Retriable<>((obj) -> {
            Status status;
            try {
                status = pollWritingTwitter.updateStatus(update);
            } catch (TwitterException te) {
                if (te.getErrorMessage().toLowerCase().contains("status is a duplicate")) {
                    // Find the duplicate, delete it, and then try posting it again.
                    try {
                        deleteDuplicates(text, inReplyToStatusId);
                        status = pollWritingTwitter.updateStatus(update);
                    } catch (TwitterException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    throw new RuntimeException("Error trying to tweet '" + text + "'", te);
                }
            }
            return status;
        }, "find and delete dupes", time).performWithRetries();
    }

    private void deleteDuplicates(String text, Long inReplyToStatusId) throws TwitterException {
        ResponseList<Status> userTimeline = pollWritingTwitter.getUserTimeline();
        for (Status tweet : userTimeline) {
            String tweetText = tweet.getText();
            long inReply = tweet.getInReplyToStatusId();
            boolean doesInReplyMatch = (inReplyToStatusId == null && inReply < 0) ||
                    (inReplyToStatusId != null && inReplyToStatusId == inReply);
            if (!tweet.isRetweet() && text.equals(tweetText) && doesInReplyMatch) {
                // Delete the tweet
                pollWritingTwitter.destroyStatus(tweet.getId());
            }
        }
    }

    public Status postPoll(int durationMinutes, String statusText, List<String> choices,
            Long inReplyToStatusId) {

        int choicesCount = choices.size();
        Map<String, String> params = new LinkedHashMap<>();
        params.put("twitter:api:api:endpoint", "1");
        params.put("twitter:card", "poll" + choices.size() + "choice_text_only");
        params.put("twitter:long:duration_minutes", Integer.toString(durationMinutes));
        ensureTwoToFourChoices(choicesCount);

        for (int i = 0; i < choices.size(); i++) {
            String entry = choices.get(i);
            params.put("twitter:string:choice" + (i + 1) + "_label", entry);
        }

        return new Retriable<>((obj) -> {
            try {
                StopWatch stopWatch = StopWatch.start("Twitter", "create card");
                CardLight card = pollWritingTwitter.createCard(params);
                stopWatch.reportAndRestart("update status with card");
                StatusUpdateWithCard update = new StatusUpdateWithCard(statusText, card
                        .getCardUri());
                if (inReplyToStatusId != null && inReplyToStatusId > 0) {
                    update.setInReplyToStatusId(inReplyToStatusId);
                }
                Status status = pollWritingTwitter.updateStatus(update);
                stopWatch.stopAndReport();

                return status;
            } catch (TwitterException te) {
                log.info("Error trying to tweet message '" + statusText +
                        "' with poll choices '" + choices + "'");
                System.out.println("TwitterException: " +
                        "error message: " + te.getErrorMessage() +
                        ", error code: " + te.getErrorCode() +
                        ", access level: " + te.getAccessLevel() +
                        ", isCausedByNetworkIssue: " + te.isCausedByNetworkIssue() +
                        ", cause: " + te.getCause());
                try {
                    Field field = TwitterException.class.getDeclaredField("response");
                    field.setAccessible(true);
                    HttpResponse response = (HttpResponse) field.get(te);
                    new Debug().logHttpResponse(response);
                } catch (NoSuchFieldException | IllegalAccessException reflectionException) {
                    reflectionException.printStackTrace();
                }
                throw new RuntimeException(te);
            }
        }, "post poll", time).performWithRetries();
    }

    private void ensureTwoToFourChoices(int choicesCount) {
        if (choicesCount < 2) {
            throw new RuntimeException("Must have at least two poll choices");
        }
        if (choicesCount > 4) {
            throw new RuntimeException("Too many poll choices (max 4)");
        }
    }

    public StatusWithCard readPreviousTweet(Long tweetId) {
        StatusWithCard tweet;
        if (tweetId == null) {
            tweet = null;
        } else {
            try {
                StopWatch stopWatch = StopWatch.start("Twitter", "read poll");
                tweet = pollReadingTwitter.showStatusWithCard(tweetId);
                stopWatch.stopAndReport();
            } catch (TwitterException e) {
                throw new RuntimeException(e);
            }
        }
        return tweet;
    }

    /**
     * If the account's twitter bio, or description, ends with " High Score: " and some digits then
     * this method parses the old high score, and if the specified new score is greater than the old
     * high score, this method updates the twitter bio with the new high score.
     *
     * @param score the score of the current game
     */
    public void updateBioForScore(int score) {
        User user;
        try {
            user = pollWritingTwitter.showUser(pollWritingConfig.getUser());
        } catch (TwitterException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        String description = user.getDescription();
        Pattern pattern = Pattern.compile("(.*) High Score: ([0-9]+)");
        Matcher matcher = pattern.matcher(description);
        if (matcher.matches()) {
            int oldHighScore = Integer.parseInt(matcher.group(2));
            if (score > oldHighScore) {
                String newDesc = matcher.group(1) + " High Score: " + score;
                String url = user.getURL();
                String location = user.getLocation();
                try {
                    pollWritingTwitter.updateProfile(user.getScreenName(), url, location, newDesc);
                    log.info("Updated twitter bio with score " + score + " so now the " +
                            "bio is '" + newDesc + "'");
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        BotConfig config = new BotConfigFactory().configure();
        Configuration readingTwitterConfig = config.getReadingTwitterConfig();
        Configuration writingTwitterConfig = config.getWritingTwitterConfig();
        Time time = new Time();
        TwitterPollMaker pollMaker = new TwitterPollMaker(readingTwitterConfig,
                writingTwitterConfig, time);
        long tweetId = 1094904585607471104L;
        StatusWithCard tweet = pollMaker.readPreviousTweet(tweetId);
        Card card = tweet.getCard();
        Choice[] choices = card.getChoices();
        for (Choice c : choices) {
            System.out.printf("%3d %s\n", c.getCount(), c.getLabel());
        }
    }
}
