package sondow.tetra.conf;

import java.time.ZonedDateTime;

public class Time {

    public ZonedDateTime nowZonedDateTime() {
        return ZonedDateTime.now();
    }

    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public void waitASec() {
        sleep(1000L);
    }
}
