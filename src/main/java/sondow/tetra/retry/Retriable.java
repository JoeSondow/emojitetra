package sondow.tetra.retry;

import com.google.common.math.IntMath;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import sondow.tetra.conf.Time;

/**
 * Mechanism for retrying actions multiple times, with customized error handling behavior.
 *
 * @param <R> the return type of the work to be done
 */
public class Retriable<R> {

    /**
     * The base of the exponent to be used for exponential backoff.
     * <p>
     * A value of 1 disables exponential backoff. A value of 2 causes retry delays to grow
     * exponentially based on powers of 2.
     */
    Integer exponentialBackoffBaseMultiplier = 2;

    /**
     * The number of times to try to perform the work before giving up and re-throwing the
     * exceptions from the attempts.
     */
    Integer maxTries = 9;

    private final Time time;

    /**
     * The work to try to execute without an exception getting thrown.
     */
    private final Function<Integer, R> work;

    /**
     * The maximum time in milliseconds to wait between attempts.
     */
    private static final Integer MAX_DELAY_MILLIS = 2 * 60 * 1000;

    /**
     * The time in milliseconds to wait after the first failed attempt. This is also the base factor
     * for subsequent retry delay lengths when using exponential backoff.
     */
    private static final Integer FIRST_DELAY_MILLIS = 250;

    /**
     * The name of the process that will be attempted, to be used in error messages.
     */
    private final String name;

    public Retriable(Function<Integer, R> work, String name, Time time) {
        this.work = work;
        this.name = name;
        this.time = time;
    }

    /**
     * Repeatedly attempts to perform some work without throwing an exception, up to a chosen number
     * of times
     *
     * @return the object returned by the work closure, with unknown type
     */
    public R performWithRetries() {
        List<Exception> exceptions = new ArrayList<>();
        for (int i = 0; i < maxTries; i++) {
            if (i > 0) {
                int multiplier = Math.max(1, exponentialBackoffBaseMultiplier);
                int calculatedDelayMillis = IntMath.pow(multiplier, i - 1) * FIRST_DELAY_MILLIS;
                int delayMillis = Math.min(calculatedDelayMillis, MAX_DELAY_MILLIS);
                delay(delayMillis);
            }
            try {
                return work.apply(i);
            } catch (Exception e) {
                exceptions.add(e);
            }
        }

        String msg = "Failed retriable process '" + name + "', max retries: " + maxTries;
        throw new CollectedExceptions(msg, exceptions);
    }

    /**
     * Does some sleeping. This method can be overridden by subclasses to substitute behavior for
     * cases such as unit testing.
     *
     * @param milliseconds the number of milliseconds to sleep
     */
    public void delay(int milliseconds) {
        time.sleep(milliseconds);
    }
}