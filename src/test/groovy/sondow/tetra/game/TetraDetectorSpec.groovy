package sondow.tetra.game

import sondow.tetra.io.Converter
import spock.lang.Specification

class TetraDetectorSpec extends Specification {

    def "should detect imminent tetra"() {
        setup:
        Converter converter = new Converter(new Random(4))
        String compressedString = '' +
                rows + // rows
                'f1500L3k98' +
                dir + // direction
                r + // row
                shp + // current shape
                c + // column
                prv + // previous move
                '22'
        Game game = converter.makeGameFromCompressedString(compressedString)
        TetraDetector detector = new TetraDetector()

        when:
        boolean isTetraImminent = detector.isFourLineClearImminent(game)
        String gameToString = game.toString()

        then:
        isTetraImminent == immin
        gameToString == gameString

        where:
        immin | dir | r | shp | c | prv | rows                      | gameString
        false | 'S' | 0 | 'J' | 3 | 'n' | '9*.7-.3T.3-L.T3OO'       | '' +
                '◽◽🍏◽◽◽◽\n' +
                '◽◽🍏🍏🍏◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍉\n' +
                '◽◽◽◽◽◽◽　🍉\n' +
                '◽◽◽◽◽◽◽　🍉🍉\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　1500\n' +
                '◽◽◽🍋◽◽◽\n' +
                '🍉◽🍋🍋🍋🍇🍇'
        true  | 'E' | 4 | 'I' | 3 | 'd' | '7*.7-4*T3.T3'            | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽🍑◽◽◽　🍉\n' +
                '◽◽◽🍑◽◽◽　🍉\n' +
                '◽◽◽🍑◽◽◽　🍉🍉\n' +
                '◽◽◽🍑◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🍋🍋🍋◽🍋🍋🍋　Score\n' +
                '🍋🍋🍋◽🍋🍋🍋　1500\n' +
                '🍋🍋🍋◽🍋🍋🍋\n' +
                '🍋🍋🍋◽🍋🍋🍋'
        true  | 'W' | 4 | 'I' | 3 | 'd' | '7*.7-4*T3.T3'            | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽🍑◽◽◽　🍉\n' +
                '◽◽◽🍑◽◽◽　🍉\n' +
                '◽◽◽🍑◽◽◽　🍉🍉\n' +
                '◽◽◽🍑◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🍋🍋🍋◽🍋🍋🍋　Score\n' +
                '🍋🍋🍋◽🍋🍋🍋　1500\n' +
                '🍋🍋🍋◽🍋🍋🍋\n' +
                '🍋🍋🍋◽🍋🍋🍋'
        true  | 'W' | 4 | 'I' | 3 | 'd' | '6*.7-5*T3.T3'            | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽🍑◽◽◽　🍉\n' +
                '◽◽◽🍑◽◽◽　🍉\n' +
                '◽◽◽🍑◽◽◽　🍉🍉\n' +
                '◽◽◽🍑◽◽◽\n' +
                '🍋🍋🍋◽🍋🍋🍋\n' +
                '🍋🍋🍋◽🍋🍋🍋　Score\n' +
                '🍋🍋🍋◽🍋🍋🍋　1500\n' +
                '🍋🍋🍋◽🍋🍋🍋\n' +
                '🍋🍋🍋◽🍋🍋🍋'
        true  | 'W' | 4 | 'I' | 0 | 'f' | '6*.7-5*.T6'              | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '🍑◽◽◽◽◽◽　🍉\n' +
                '🍑◽◽◽◽◽◽　🍉\n' +
                '🍑◽◽◽◽◽◽　🍉🍉\n' +
                '🍑◽◽◽◽◽◽\n' +
                '◽🍋🍋🍋🍋🍋🍋\n' +
                '◽🍋🍋🍋🍋🍋🍋　Score\n' +
                '◽🍋🍋🍋🍋🍋🍋　1500\n' +
                '◽🍋🍋🍋🍋🍋🍋\n' +
                '◽🍋🍋🍋🍋🍋🍋'
        true  | 'W' | 4 | 'I' | 6 | 'i' | '6*.7-5*T6.'              | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽🍑　🍉\n' +
                '◽◽◽◽◽◽🍑　🍉\n' +
                '◽◽◽◽◽◽🍑　🍉🍉\n' +
                '◽◽◽◽◽◽🍑\n' +
                '🍋🍋🍋🍋🍋🍋◽\n' +
                '🍋🍋🍋🍋🍋🍋◽　Score\n' +
                '🍋🍋🍋🍋🍋🍋◽　1500\n' +
                '🍋🍋🍋🍋🍋🍋◽\n' +
                '🍋🍋🍋🍋🍋🍋◽'
        false | 'W' | 4 | 'I' | 6 | 'i' | '6*.7-.J6-4*T6.'          | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽⬛　🍉\n' +
                '◽◽◽◽◽◽⬛　🍉\n' +
                '◽◽◽◽◽◽⬛　🍉🍉\n' +
                '◽◽◽◽◽◽⬛\n' +
                '◽🍏🍏🍏🍏🍏🍏\n' +
                '🍋🍋🍋🍋🍋🍋◽　Score\n' +
                '🍋🍋🍋🍋🍋🍋◽　1500\n' +
                '🍋🍋🍋🍋🍋🍋◽\n' +
                '🍋🍋🍋🍋🍋🍋◽'
        false | 'W' | 4 | 'I' | 6 | 'i' | '6*.7-2*T6-.J.J.J.-2*T6.' | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽🍑　🍉\n' +
                '◽◽◽◽◽◽🍑　🍉\n' +
                '◽◽◽◽◽◽🍑　🍉🍉\n' +
                '◽◽◽◽◽◽🍑\n' +
                '🍋🍋🍋🍋🍋🍋◽\n' +
                '🍋🍋🍋🍋🍋🍋◽　Score\n' +
                '◽🍏◽🍏◽🍏◽　1500\n' +
                '🍋🍋🍋🍋🍋🍋◽\n' +
                '🍋🍋🍋🍋🍋🍋◽'
        true  | 'W' | 4 | 'I' | 0 | 'i' | '5*.7-.4I3-4*.T6-L3.J3'   | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '🍑◽◽◽◽◽◽　🍉\n' +
                '🍑◽◽◽◽◽◽　🍉\n' +
                '🍑◽◽◽◽◽◽　🍉🍉\n' +
                '🍑◽◽◽🍑🍑🍑\n' +
                '◽🍋🍋🍋🍋🍋🍋\n' +
                '◽🍋🍋🍋🍋🍋🍋　Score\n' +
                '◽🍋🍋🍋🍋🍋🍋　1500\n' +
                '◽🍋🍋🍋🍋🍋🍋\n' +
                '🍉🍉🍉◽🍏🍏🍏'
        false | 'W' | 4 | 'I' | 0 | 'i' | '5*.7-.4I3-4*.T6-.L3.J2'  | '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '🍑◽◽◽◽◽◽　🍉\n' +
                '🍑◽◽◽◽◽◽　🍉\n' +
                '🍑◽◽◽◽◽◽　🍉🍉\n' +
                '🍑◽◽◽🍑🍑🍑\n' +
                '◽🍋🍋🍋🍋🍋🍋\n' +
                '◽🍋🍋🍋🍋🍋🍋　Score\n' +
                '◽🍋🍋🍋🍋🍋🍋　1500\n' +
                '◽🍋🍋🍋🍋🍋🍋\n' +
                '◽🍉🍉🍉◽🍏🍏'
    }
}
