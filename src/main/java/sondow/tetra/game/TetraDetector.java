package sondow.tetra.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import sondow.tetra.shape.Shape;
import sondow.tetra.shape.ShapeType;

/**
 * This class analyzes a Game object to see if the next move can be (and therefore will be) a tetra,
 * a four-line clear. This is only possible when the current piece is a line oriented east or west,
 * where the poll choices include Plummet, and the static tiles in the grid have a four-line-tall
 * well on four consecutive rows, each row of which is completely filled except for the well column,
 * and the well column is the same column the piece is in, and there are no static tiles above the
 * well.
 */
public class TetraDetector {

    public boolean isFourLineClearImminent(Game game) {
        boolean tetraIsImminent = false;
        Shape piece = game.getPiece();
        boolean isLine = ShapeType.LINE.equals(piece.getShapeType());
        if (isLine) {
            boolean isEastOrWest = Arrays.asList(Direction.EAST, Direction.WEST)
                    .contains(piece.getDirection());
            if (isEastOrWest) {
                boolean canPlummet = game.listLegalMoves().contains(Move.PLUMMET);
                if (canPlummet) {
                    List<List<Tile>> rows = game.getRows();
                    tetraIsImminent = containExposedWellBeneathPiece(rows, piece);
                }
            }
        }
        return tetraIsImminent;
    }

    private boolean containExposedWellBeneathPiece(List<List<Tile>> rows, Shape linePiece) {
        int pieceColumnIndex = linePiece.getColumnIndex();
        List<Integer> indicesOfRowsWithPerfectGap = new ArrayList<>();
        for (int i = 0; i < rows.size(); i++) {
            List<Tile> row = rows.get(i);
            boolean hasEmptySpaceInPieceColumn = (row.get(pieceColumnIndex) == null);
            if (hasEmptySpaceInPieceColumn) {
                boolean hasGapOnlyBelowPiece = doesRowHaveGapOnlyBelowPiece(row, pieceColumnIndex);
                if (hasGapOnlyBelowPiece) {
                    indicesOfRowsWithPerfectGap.add(i);
                }
            }
        }

        // Are there four consecutive rows with perfect gaps that are otherwise full?
        Integer previous = null;
        Set<Integer> consecutiveRowIndices = new TreeSet<>();
        for (Integer rowIndex : indicesOfRowsWithPerfectGap) {
            if (previous != null) {
                if (rowIndex - previous == 1) {
                    consecutiveRowIndices.add(previous);
                    consecutiveRowIndices.add(rowIndex);
                } else { // Rows are not consecutive
                    consecutiveRowIndices = new TreeSet<>(); // Start counting all over again
                }
            }
            previous = rowIndex;
        }
        boolean wellIsBlocked = false;
        boolean exposedTetraWellExistsBelowLinePiece = false;
        if (consecutiveRowIndices.size() >= 4) {
            // Is there anything above the well?
            // (Disregard rare case of overhang far above well. It might never happen. If it ever
            // happens and we miss it, that's okay.
            Iterator<Integer> iterator = consecutiveRowIndices.iterator();
            Integer earliestRowOfWell = iterator.next();
            for (int i = 0; i < earliestRowOfWell; i++) {
                List<Tile> row = rows.get(i);
                Tile tile = row.get(pieceColumnIndex);
                if (tile != null) {
                    wellIsBlocked = true;
                }
            }
            if (!wellIsBlocked) {
                exposedTetraWellExistsBelowLinePiece = true;
            }

            // Are there rows below the consecutive rows, where the piece could continue falling
            // but those lower rows are incomplete so it won't be a tetra?
            int nextRowOfWell;
            do {
                nextRowOfWell = iterator.next();
            } while (iterator.hasNext());
            int lastRowOfWell = nextRowOfWell;
            if (lastRowOfWell < rows.size() - 1) {
                int belowWell = lastRowOfWell + 1;
                List<Tile> row = rows.get(belowWell);
                boolean rowBelowWellFitsLinePiece = (row.get(pieceColumnIndex) == null);
                boolean rowHasMoreBlanks = (row.stream().filter(Objects::isNull).count() >= 2);
                if (rowBelowWellFitsLinePiece && rowHasMoreBlanks) {
                    exposedTetraWellExistsBelowLinePiece = false;
                }
            }
        }

        return exposedTetraWellExistsBelowLinePiece;
    }

    private boolean doesRowHaveGapOnlyBelowPiece(List<Tile> row, int pieceColumnIndex) {
        int emptySpots = 0;
        boolean hasGapBelowPiece = false;
        for (int i = 0; i < row.size() && emptySpots <= 1; i++) {
            Tile tile = row.get(i);
            if (tile == null) {
                emptySpots++;
                if (i == pieceColumnIndex) {
                    hasGapBelowPiece = true;
                }
            }
        }
        return emptySpots == 1 && hasGapBelowPiece;
    }
}
