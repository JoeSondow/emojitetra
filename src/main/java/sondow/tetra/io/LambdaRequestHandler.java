package sondow.tetra.io;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import sondow.tetra.conf.StopWatch;
import sondow.tetra.game.Player;

/**
 * The function that AWS Lambda will invoke.
 *
 * @author @JoeSondow
 */
@SuppressWarnings("unused")
public class LambdaRequestHandler implements RequestHandler<Object, Object> {

    /*
     * (non-Javadoc)
     *
     * @see com.amazonaws.services.lambda.runtime.RequestHandler#handleRequest(java. lang.Object,
     * com.amazonaws.services.lambda.runtime.Context)
     */
    @Override
    public Object handleRequest(Object input, Context context) {
        StopWatch stopWatch = StopWatch.start("LambdaRequestHandler", "full run");
        Outcome outcome = new Player().play();
        stopWatch.stopAndReport();
        return outcome;
    }

    public static void main(String[] args) {
        new LambdaRequestHandler().handleRequest(null, null);
    }
}
