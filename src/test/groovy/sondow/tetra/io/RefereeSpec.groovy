package sondow.tetra.io

import sondow.tetra.game.FakeCard
import spock.lang.Specification
import twitter4j.Choice

class RefereeSpec extends Specification {

    def "poll should have super majority iff 70%+ winner or 40%+ difference between winners"() {
        setup:
        Referee referee = new Referee()
        FakeCard card = new FakeCard()

        Choice left = new Choice("Left", lft)
        Choice right = new Choice("Right", rit)
        Choice rotate = new Choice("Rotate", rot)
        Choice down = new Choice("Down", dn)
        Choice[] choices = [left, right, rotate, down].toArray(new Choice[0])
        card.setChoices(choices)

        when:
        boolean hasSuperMajority = referee.hasSuperMajority(card)

        then:
        hasSuperMajority == isSuperMajority

        where:
        lft | rit | rot | dn | isSuperMajority
        0   | 0   | 0   | 0  | false
        0   | 0   | 1   | 0  | false
        0   | 0   | 9   | 0  | false
        0   | 0   | 10  | 0  | true // Need at least 10 votes total to get supermajority
        23  | 8   | 41  | 12 | false
        10  | 70  | 0   | 20 | true
        0   | 30  | 70  | 0  | false
        5   | 15  | 80  | 0  | true
        5   | 5   | 90  | 0  | true
        30  | 1   | 69  | 0  | false
        10  | 10  | 69  | 11 | true
        50  | 0   | 40  | 10 | false
        150 | 15  | 21  | 6  | true
    }
}
