package sondow.tetra.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Move {
    LEFT("⬅️ Left", "lf", "f"),
    RIGHT("➡️ Right", "ri", "i"),
    LEFT_OR_RIGHT("↔️ Left or Right", "lr", "o"),
    ROTATE("🔄 Rotate", "ro", "a"),
    DOWN("⬇️ Down", "dn", "d"),
    STOP("⬇️ Stop", "st", "s"),
    PLUMMET("⏬ Plummet", "pl", "p");

    /**
     * The code for when there is no previous Move because it's still the first game turn.
     */
    public static final String NULL_CODE = "n";

    private final String emojiAndTitleCase;
    private final String abbreviation;
    private final String code;
    private final String lowercaseLabel;

    static List<Move> LEFT_RIGHT_ROTATE_DOWN = Arrays.asList(LEFT, RIGHT, ROTATE, DOWN);
    static List<Move> LEFTORRIGHT_ROTATE_DOWN = Arrays.asList(LEFT_OR_RIGHT, ROTATE, DOWN);

    Move(String emojiAndTitleCase, String abbreviation, String code) {
        this.emojiAndTitleCase = emojiAndTitleCase;
        this.abbreviation = abbreviation;
        this.code = code;
        this.lowercaseLabel = Move.lowercaseLettersOnly(emojiAndTitleCase);
    }

    String getEmojiAndTitleCase() {
        return emojiAndTitleCase;
    }

    String getLowercaseLabel() {
        return lowercaseLabel;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getCode() {
        return code;
    }

    private static final Map<String, Move> ABBREVIATION_TO_MOVE = new HashMap<>();

    private static final Map<String, Move> CODE_TO_MOVE = new HashMap<>();

    private static final Map<String, Move> LOWERCASE_LABEL_TO_MOVE = new HashMap<>();

    /**
     * Chooses the matching Move for a specified label. Case-insensitive. All letters must match but
     * all non-letter characters will be disregarded. This allows for Twitter's occasional changes
     * to how it outputs emoji characters in poll choices to some Android and Java clients.
     * <p>
     * This makes the text-matching algorithm more robust against future unexpected Twitter changes
     * about which emoji characters they output to various clients, like when they stopped revealing
     * boxed arrow characters in poll choice labels to some Android and Java clients on Sep 13,
     * 2022, which broke this poll bot.
     * <p>
     * Only letters get compared. Numerals, emojis, whitespace, punctuation are all ignored.
     *
     * @param label the string to look up
     * @return the matching Move
     */
    public static Move fromLabel(String label) {
        String lowercaseLetters = lowercaseLettersOnly(label);
        return LOWERCASE_LABEL_TO_MOVE.get(lowercaseLetters);
    }

    public static String lowercaseLettersOnly(String input) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (Character.isLetter(c)) {
                char lower = Character.toLowerCase(c);
                builder.append(lower);
            }
        }
        return builder.toString();
    }

    static {
        // Legacy abbreviations until shorter codes are ubiquitous
        for (Move move : Move.values()) {
            ABBREVIATION_TO_MOVE.put(move.getAbbreviation(), move);
            CODE_TO_MOVE.put(move.getCode(), move);
            LOWERCASE_LABEL_TO_MOVE.put(move.getLowercaseLabel(), move);
        }
    }

    /**
     * Tries to find a matching Move for a given abbreviation string, probably from database.
     *
     * @param abbreviation the abbreviation to look up
     * @return the matching Move
     */
    public static Move fromAbbreviation(String abbreviation) {
        return ABBREVIATION_TO_MOVE.get(abbreviation);
    }

    /**
     * Tries to find a matching Move for a given code string, probably from database.
     *
     * @param code the code to look up
     * @return the matching Move, or null if no matching Move because it's the first game turn
     */
    public static Move fromCode(String code) {
        return CODE_TO_MOVE.get(code);
    }

    /**
     * Poll choice strings for moves show an emojiAndTitleCase, followed by a space, followed by the
     * title case version of the move name.
     *
     * @param moves the moves to convert
     * @return the list of string representations of the moves, with emojis included
     */
    public static List<String> toPollChoices(List<Move> moves) {
        List<String> choices = new ArrayList<>();
        for (Move move : moves) {
            choices.add(move.getEmojiAndTitleCase());
        }
        return choices;
    }
}
