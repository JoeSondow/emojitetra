package sondow.tetra.io

import sondow.tetra.conf.Time
import sondow.tetra.retry.CollectedExceptions
import spock.lang.Specification
import twitter4j.CardLight
import twitter4j.CardsTwitterImpl
import twitter4j.ResponseList
import twitter4j.Status
import twitter4j.StatusUpdate
import twitter4j.StatusUpdateWithCard
import twitter4j.TwitterException
import twitter4j.User
import twitter4j.conf.Configuration

class TwitterPollMakerSpec extends Specification {

    def 'should tweet non-poll message if no error'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl, Mock(Time))
        StatusUpdate statusUpdate = new StatusUpdate("Hello")
        Status status = Mock()

        when:
        twitterPollMaker.tweet("Hello", null, null)

        then:
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> status
        0 * _._
    }

    def 'should tweet non-poll message with braille blank appended if duplicate'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl, Mock(Time))
        StatusUpdate statusUpdate = new StatusUpdate("Hello")
        statusUpdate.setInReplyToStatusId(456)
        TwitterException dupeException = new TwitterException('' +
                '{"errors":[{"message":"Status is a duplicate.","code":999}]}')
        Status status = Mock()
        ResponseList<Status> responseList = new FakeResponseList([
                new Tweet(id: 72, text: 'Hello', inReplyToStatusId: 456),
                new Tweet(id: 456, text: 'Goodbye', inReplyToStatusId: 333)
        ])

        when:
        twitterPollMaker.tweet("Hello", 456, null)

        then:
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> { throw dupeException }
        1 * writingCardsTwitterImpl.getUserTimeline() >> responseList
        1 * writingCardsTwitterImpl.destroyStatus(72)
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> status
        0 * _._
    }

    def 'should tweet non-poll message with two braille blanks if one was already done'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl, Mock(Time))
        StatusUpdate statusUpdate = new StatusUpdate("Hello")
        statusUpdate.setInReplyToStatusId(456)
        TwitterException dupeException = new TwitterException('' +
                '{"errors":[{"message":"Status is a duplicate.","code":999}]}')
        Status status = Mock()
        ResponseList<Status> responseList = new FakeResponseList([
                new Tweet(id: 72, text: 'Hello', inReplyToStatusId: 456)
        ])

        when:
        twitterPollMaker.tweet("Hello", 456, null)

        then:
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> { throw dupeException }
        1 * writingCardsTwitterImpl.getUserTimeline() >> responseList
        1 * writingCardsTwitterImpl.destroyStatus(72)
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> status
        0 * _._
    }

    def 'should tweet poll if no error'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl, Mock(Time))
        StatusUpdateWithCard statusUpdate = new StatusUpdateWithCard("Hello", 'some_card_uri')
        statusUpdate.inReplyToStatusId = 349L
        Status status = Mock()
        CardLight card = Mock()
        Map<String, String> params = ['twitter:api:api:endpoint'     : '1',
                                      'twitter:card'                 : 'poll2choice_text_only',
                                      'twitter:long:duration_minutes': '20',
                                      'twitter:string:choice1_label' : 'Red',
                                      'twitter:string:choice2_label' : 'Blue']

        when:
        twitterPollMaker.postPoll(20, "Hello", ['Red', 'Blue'], 349L)

        then:
        1 * writingCardsTwitterImpl.createCard(params) >> card
        1 * card.getCardUri() >> 'some_card_uri'
        1 * writingCardsTwitterImpl.updateStatus(statusUpdate) >> status
        0 * _._
    }

    static String longBio = 'I am a bot created by @JoeSondow. Vote on the next move. Not to be ' +
            'confused with any trademarked block-fitting game. #TeamPlummet #TeamRotate. High ' +
            'Score: 13850'
    static String longBio15k = 'I am a bot created by @JoeSondow. Vote on the next move. Not to ' +
            'be confused with any trademarked block-fitting game. #TeamPlummet #TeamRotate. High ' +
            'Score: 15000'

    def "should update bio with new high score"() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingCardsTwitterImpl = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingCardsTwitterImpl, Mock(Time))
        User user = Mock()
        String url = 'gitlab.com/emojitetra'

        when:
        twitterPollMaker.updateBioForScore(newScore)

        then:
        1 * writingConfig.getUser() >> 'Sammy'
        1 * writingCardsTwitterImpl.showUser('Sammy') >> user
        1 * user.getDescription() >> oldDesc
        updates * user.getLocation() >> null
        updates * user.getURL() >> url
        updates * user.getScreenName() >> 'Sammy'
        updates * writingCardsTwitterImpl.updateProfile('Sammy', url, null, newDesc)
        0 * _._

        where:
        newScore | updates | oldDesc                  | newDesc
        230      | 1       | 'Wassup High Score: 100' | 'Wassup High Score: 230'
        90       | 0       | 'Wassup High Score: 100' | 'Wassup High Score: 100'
        90       | 0       | 'Wassup High Score: 100' | 'Wassup High Score: 100'
        1400     | 0       | 'I love you'             | 'I love you'
        15000    | 1       | longBio                  | longBio15k
    }

    def 'should retry failed poll posts with exponential backoff'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingImpl = Mock()
        Time time = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingImpl, time)
        StatusUpdateWithCard statusUpdate = new StatusUpdateWithCard("Hello", 'some_card_uri')
        statusUpdate.inReplyToStatusId = 349L
        CardLight card = Mock()
        Map<String, String> params = ['twitter:api:api:endpoint'     : '1',
                                      'twitter:card'                 : 'poll2choice_text_only',
                                      'twitter:long:duration_minutes': '20',
                                      'twitter:string:choice1_label' : 'Red',
                                      'twitter:string:choice2_label' : 'Blue']
        int tries = 1;

        when:
        twitterPollMaker.postPoll(20, "Hello", ['Red', 'Blue'], 349L)

        then:
        9 * writingImpl.createCard(params) >> card
        9 * card.getCardUri() >> 'some_card_uri'
        9 * writingImpl.updateStatus(statusUpdate) >>
                {
                    throw new TwitterException('{"errors":[{"message":"fake poll post fail ' +
                            tries++ + '","code":420}]}')
                }
        1 * time.sleep(250)
        1 * time.sleep(500)
        1 * time.sleep(1000)
        1 * time.sleep(2000)
        1 * time.sleep(4000)
        1 * time.sleep(8000)
        1 * time.sleep(16000)
        1 * time.sleep(32000)
        thrown CollectedExceptions
        0 * _._
    }

    def 'should retry failed card creation with exponential backoff'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingImpl = Mock()
        Time time = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingImpl, time)
        StatusUpdateWithCard statusUpdate = new StatusUpdateWithCard("Hello", 'some_card_uri')
        statusUpdate.inReplyToStatusId = 349L
        Map<String, String> params = ['twitter:api:api:endpoint'     : '1',
                                      'twitter:card'                 : 'poll2choice_text_only',
                                      'twitter:long:duration_minutes': '20',
                                      'twitter:string:choice1_label' : 'Red',
                                      'twitter:string:choice2_label' : 'Blue']
        int tries = 1;

        when:
        twitterPollMaker.postPoll(20, "Hello", ['Red', 'Blue'], 349L)

        then:
        9 * writingImpl.createCard(params) >>
                {
                    throw new TwitterException('{"errors":[{"message":"fake card create fail ' +
                            tries++ + '","code":420}]}')
                }
        1 * time.sleep(250)
        1 * time.sleep(500)
        1 * time.sleep(1000)
        1 * time.sleep(2000)
        1 * time.sleep(4000)
        1 * time.sleep(8000)
        1 * time.sleep(16000)
        1 * time.sleep(32000)
        thrown CollectedExceptions
        0 * _._
    }

    def 'should retry failed card creation with exponential backoff until success'() {
        setup:
        Configuration readingConfig = Mock()
        CardsTwitterImpl readingCardsTwitterImpl = Mock()
        Configuration writingConfig = Mock()
        CardsTwitterImpl writingImpl = Mock()
        Time time = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(readingConfig,
                readingCardsTwitterImpl, writingConfig, writingImpl, time)
        StatusUpdateWithCard statusUpdate = new StatusUpdateWithCard("Hello", 'some_card_uri')
        statusUpdate.inReplyToStatusId = 349L
        CardLight card = Mock()
        Map<String, String> params = ['twitter:api:api:endpoint'     : '1',
                                      'twitter:card'                 : 'poll2choice_text_only',
                                      'twitter:long:duration_minutes': '20',
                                      'twitter:string:choice1_label' : 'Red',
                                      'twitter:string:choice2_label' : 'Blue']
        int tries = 0
        Status status = Mock()

        when:
        twitterPollMaker.postPoll(20, "Hello", ['Red', 'Blue'], 349L)

        then:
        4 * writingImpl.createCard(params) >> {
            tries++
            if (tries >= 4) {
                return card
            } else {
                throw new TwitterException('{"errors":[{"message":"fake card create fail ' + tries +
                        '","code":69}]}')
            }
        }
        1 * card.getCardUri() >> 'some_card_uri'
        1 * writingImpl.updateStatus(statusUpdate) >> status
        1 * time.sleep(250)
        1 * time.sleep(500)
        1 * time.sleep(1000)
        0 * _._
    }
}
